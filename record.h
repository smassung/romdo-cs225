#ifndef RECORD_H
#define RECORD_H

#include <map>
#include <string>
#include <utility>

#include "util.h"

using namespace std;

/**
 * A column-based record/row in a table
 * Contains a key and various columns
 */
class Record
{
    public:
    // Public constants
    static const string emptyValue;

    public:
    // Basic Object Functions
    Record() { }
    bool operator==(const Record & other) const;
    bool operator!=(const Record & other) const { return !operator==(other); }

    // Data Manipulation Functions
    const string & key() const { return key_; }
    void key(const string & newKey) { key_ = newKey; }
    string & operator[](const string & columnHeader);
    const string & operator[](const string & columnHeader) const;
    void erase(const string & columnHeader);
    //!!unimplemented vector<string> headers() const;

    // Higher-level Manipulation Functions
    void mergeValuesFrom(const Record & otherRec, bool mergeIfHigher);
    Record evaluate() const;

    // Display functions
    void print(ostream & out) const;

    private:
    // Private Member Variables
    typedef map<string, string> ColumnMap;
    string key_;
    ColumnMap columnValues_;
};

#endif // RECORD_H

