#include <iomanip>
#include <cmath>
#include <sys/ioctl.h>
#include <utility>
#include <limits>
#include <map>
#include "distance.h"
#include "cluster.h"
#include "romdo.h"

using std::make_pair;
using std::multimap;
using std::setw;

/**
 * Merges the tables in 'files' into this instance's main table
 */
void Romdo::merge(const vector<string> & files)
{
    vector<Table> tables(files.size());
    for (size_t i = 0; i < files.size(); i++)
        tables[i].readFromFile(files[i]);
    
    merge(tables, false);
}

/**
 * Merges the tables in 'files' into this instance's main table.
 * Old values are overwritten if the new values are higher.
 */
void Romdo::mergehigher(const vector<string> & files)
{
    vector<Table> tables(files.size());
    for (size_t i = 0; i < files.size(); i++)
        tables[i].readFromFile(files[i]);
    
    merge(tables, true);
}

/**
 * Merges the tables in 'tables' into this instance's main table
 */
void Romdo::merge(const vector<Table> & tables, bool mergeIfHigher)
{
    for (size_t table_i = 0; table_i < tables.size(); table_i++)
        mainTable.mergeRecordsFrom(tables[table_i], mergeIfHigher);
}

void Romdo::sort(const string & column) const
{
    vector<pair<string, double>> vals;
    for(auto & k: mainTable.keys())
    {
        Record evaluated = mainTable.get(k).evaluate();
        stringstream sstr(evaluated[column]);
        double val;
        sstr >> val;
        vals.push_back(make_pair(k, val));
    }

    std::sort(vals.begin(), vals.end(),
        [](const pair<string, double> & a, const pair<string, double> & b) {
            return a.second > b.second;
        }
    );

    for(auto & p: vals)
    {
        cout << p.second << Table::delimiter << "("
             << p.first << ")" << endl;
    }
}

void Romdo::showall(const string & column) const
{
    vector<string> keys = mainTable.keys();
    for (size_t i = 0; i < keys.size(); i++)
        show(keys[i], column);
}

/**
 * Display the record with key 'key'
 */
void Romdo::show(const string & key, const string & column /* = "" */) const
{
    if(!checkKey(key))
        return;

    const Record & rec = mainTable.get(key);
    Record evaluated_rec = rec.evaluate();
    if (column == "")
        evaluated_rec.print(cout);
    else
    {
        cout << evaluated_rec.key() << Table::delimiter
             << evaluated_rec[column] << endl;
    }
}

bool Romdo::checkKey(const string & key) const
{
    vector<string> keys = mainTable.keys();
    if(find(keys.begin(), keys.end(), key) != keys.end())
        return true;

    multimap<size_t, string> distances;
    for(auto it = keys.begin(); it != keys.end(); ++it)
        distances.insert(make_pair(levenshteinDistance(*it, key), *it));

    cout << endl << "Uhoh! The key you entered does not exist in the table." << endl
         << "Did you perhaps mean:" << endl;

    size_t numMatches = 8;
    size_t i = 0;
    for(auto it = distances.begin(); it != distances.end() && i < numMatches; ++i, ++it)
        cout << "    " << it->second << endl;

    cout << endl;
    return false;
}

/**
 * Updates the record associated with the given key, updating its value for
 * the given column header.
 *
 * @param key Key to find our record with.
 * @param column Column header value to update.
 * @param value Value to update the column with.
 */
void Romdo::update(const string & key, const string & column, const string & value)
{
    if(!checkKey(key))
        return;

    mainTable.addHeader(column);
    Record & rec = mainTable.get(key);
    rec[column] = value;
}



void Romdo::updateall(const string & column, const string & value)
{
    vector<string> keys = mainTable.keys();
    for (size_t i = 0; i < keys.size(); i++)
        update(keys[i], column, value);
}


void Romdo::deletecolumn(const string & column)
{
    mainTable.removeHeader(column);
}

/**
 * Clusters scores for a given column.
 * @param column The column to cluster
 * @param numClusters How many clusters to make.
 */
void Romdo::cluster(const string & column, const string & numClusters) const
{
    size_t k;
    istringstream(numClusters) >> k;
    vector<pair<string, double>> scores = getScores(column);
    vector<Cluster> clusters;
    for(auto elem = scores.begin(); elem != scores.end(); ++elem)
        clusters.push_back(Cluster(*elem));

    std::sort(clusters.begin(), clusters.end());
    while(clusters.size() > k)
        mergeClosest(clusters);

    // display results
    for(auto clus = clusters.begin(); clus != clusters.end(); ++clus)
        clus->print();
}

void Romdo::mergeClosest(vector<Cluster> & clusters) const
{
    double minWidth = std::numeric_limits<double>::max();
    size_t mergeIdx = 0;
    for(size_t i = 0; i < clusters.size() - 1; ++i)
    {
        double curWidth = clusters[i + 1].average() - clusters[i].average();
        if(curWidth < minWidth)
        {
            minWidth = curWidth;
            mergeIdx = i;
        }
    }
    clusters[mergeIdx].mergeWith(clusters[mergeIdx + 1]);
    clusters.erase(clusters.begin() + mergeIdx + 1);
}

/**
 * This function is almost duplicated in histogram
 */
vector<pair<string,double>> Romdo::getScores(const string & column) const
{
    vector<pair<string, double>> scores;
    vector<string> keys = mainTable.keys();
    scores.reserve(keys.size());
    
    for (size_t i = 0; i < keys.size(); i++)
    {
        string entry = mainTable.get(keys[i]).evaluate()[column];
        if (entry != Record::emptyValue)
        {
            stringstream ss(entry);
            double validEntry = 0.0;
            if ((ss >> validEntry) && validEntry >= 0.0)
                scores.push_back(make_pair(keys[i], validEntry));
        }
    }

    return scores;
}

void Romdo::histogram(const string & column)
{
    vector<double> scores;
    vector<string> keys = mainTable.keys();
    scores.reserve(keys.size());
    size_t numZero = 0;
    double totalScore = 0;
    for (size_t i = 0; i < keys.size(); i++)
    {
        string entry = mainTable.get(keys[i]).evaluate()[column];
        if (entry != Record::emptyValue)
        {
            stringstream ss(entry);
            double validEntry = 0.0;
            if ((ss >> validEntry) && validEntry >= 0.0)
            {
                scores.push_back(validEntry);
                totalScore += validEntry;
                if(validEntry == 0)
                    ++numZero;
            }
        }
    }

    double maxScore = *std::max_element(scores.begin(), scores.end());

    // by default, set increment to 5.0. But, if this will make too few columns,
    //   set the increment to make 20 columns (zoom histogram into data range)
    double increment = 5.0;
    if(maxScore / increment < 10.0)
        increment = maxScore / 20.0;
    
    vector<size_t> counts(std::max(0L, static_cast<ssize_t>(maxScore/increment)) + 1);
    
    for (size_t i = 0; i < scores.size(); i++)
        counts[std::max(0L, static_cast<ssize_t>(scores[i]/increment))]++;

    // create bucket headers
    vector<string> bucketHeaders;
    for (size_t row = 0; row < counts.size(); row++)
    {
        stringstream bucketHeaderSS;
        bucketHeaderSS << (row * increment) << '+';
        string nextBucketHeader;
        bucketHeaderSS >> nextBucketHeader;
        bucketHeaders.push_back(nextBucketHeader);
    }
    
    // Determine bucket heading size
    size_t maxBucketHeaderLength = 0;
    size_t maxCountLength = 0;
    for (size_t row = 0; row < counts.size(); row++)
    {
        maxBucketHeaderLength = std::max(maxBucketHeaderLength, bucketHeaders[row].length());
        maxCountLength = std::max(maxCountLength, counts[row]);
    }

    // resize bucket headers to max size
    for (size_t row = 0; row < counts.size(); row++)
        bucketHeaders[row].resize(maxBucketHeaderLength, ' ');

    cout << column << " histogram:" << endl;
    double width = getTerminalWidth() - 4 - maxBucketHeaderLength;
    cout << string(width + maxBucketHeaderLength + 3, '=') << endl;
    for (size_t row = 0; row < counts.size(); row++)
    {
        int len = width * counts[row] / maxCountLength;
        if(len == 0 && counts[row] != 0)
            len = 1;
        cout << bucketHeaders[row] << " | " << string(len, 'x') << endl;
    }

    cout << string(width + maxBucketHeaderLength + 3, '=') << endl;
    cout << scores.size() << " total records, " << scores.size() - numZero << " nonzero" << endl;

    if(scores.size() == 0)
        return;

    double mean = totalScore / scores.size();
    std::sort(scores.begin(), scores.end());
    double minScore = scores[0];
    double stddev = getStddev(scores, mean);
    double median = scores[scores.size()/2];

    double nzMean = totalScore / (scores.size() - numZero);
    vector<double>::iterator end = remove(scores.begin(), scores.end(), 0.0);
    vector<double> nonZeroScores(scores.begin(), end);
    double nzMinScore = nonZeroScores[0];
    double nzStddev = getStddev(nonZeroScores, nzMean);
    double nzMedian = nonZeroScores[nonZeroScores.size()/2];

    cout << "Mean:   " << setw(6) << setprecision(4) << mean << string(12, ' ')
         << "Nonzero mean:   "  << setw(6) << setprecision(4) << nzMean << string(12, ' ')
         << "Max:         " << setw(6) << maxScore << endl;
    cout << "Median: " << setw(6) << setprecision(4) << median << string(12, ' ')
         << "Nonzero median: " << setw(6) << setprecision(4) << nzMedian << string(12, ' ')
         << "Nonzero min: " << setw(6) << nzMinScore << endl;
    cout << "Stddev: " << setw(6) << setprecision(4) << stddev << string(12, ' ')
         << "Nonzero stddev: "  << setw(6) << setprecision(4) << nzStddev << string(12, ' ')
         << "Min:         " <<  setw(6) << minScore << endl;
}

double Romdo::getStddev(const vector<double> & scores, double mean) const
{
    double squareSum = 0.0;
    for(vector<double>::const_iterator it = scores.begin(); it != scores.end(); ++it)
        squareSum += (*it) * (*it);
    return std::sqrt((squareSum/scores.size()) - mean * mean);
}

// http://stackoverflow.com/questions/1022957/getting-terminal-width-in-c
size_t Romdo::getTerminalWidth() const
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return w.ws_col;
}

void Romdo::list(const vector<string> & params) const
{
    if (params.size() != 0)
        throw util::Exception("Too many arguments to 'list'");
    list();
}


void Romdo::list() const
{
    vector<string> keys = mainTable.keys();
    for (size_t i = 0; i < keys.size(); i++)
        cout << keys[i] << endl;
}

