#include "cluster.h"

Cluster::Cluster(const string & name, double value):
    _elems(vector<pair<string, double>>())
{
    _elems.push_back(make_pair(name, value));
    calcAvg();
}

Cluster::Cluster(pair<string, double> elem):
    _elems(vector<pair<string, double>>())
{
    _elems.push_back(elem);
    calcAvg();
}

Cluster::Cluster(const vector<pair<string, double>> & elems):
    _elems(elems)
{
    calcAvg();
}

void Cluster::mergeWith(const Cluster & other)
{
    _elems.insert(_elems.begin(), other._elems.begin(), other._elems.end());
    calcAvg();
}

void Cluster::calcAvg()
{
    double sum = 0.0;
    for(auto it = _elems.begin(); it != _elems.end(); ++it)
        sum += it->second;
    _avg = sum / _elems.size();
}

double Cluster::average() const
{
    return _avg;
}

size_t Cluster::numElements() const
{
    return _elems.size();
}

bool Cluster::operator<(const Cluster & other) const
{
    return average() < other.average();
}

void Cluster::print() const
{
    cout << "Cluster size: " << _elems.size() << " Average: " << _avg << endl;
    for(auto p = _elems.begin(); p != _elems.end(); ++p)
        cout << "    " << p->second << " (" << p->first << ")" << endl;
}
