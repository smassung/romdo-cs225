CC = g++
CFLAGS = -Wall -Wshadow
ROMDOEXE = romdo
ROMDOOBJS := main.o romdo_main.o romdo.o util.o table.o record.o evaluate.o cluster.o
OPTIMIZE = off

ifeq ($(strip $(OPTIMIZE)),on)
CFLAGS += -O2 -DOPTIMIZE
else ifeq ($(strip $(OPTIMIZE)),off)
CFLAGS += -g -O0
else
$(warning Invalid value specified for OPTIMIZE. Should be on or off)
CFLAGS += -g -O0
endif

CFLAGS += -std=c++0x

ALL_TARGETS = $(ROMDOEXE)
all: $(ALL_TARGETS)

$(ROMDOEXE): $(ROMDOOBJS) Makefile liblua/liblua.a
	$(CC) $(ROMDOOBJS) -L./liblua/ -llua -o $@

romdo_main.o : romdo_main.cpp $(wildcard *.h) README_ROMDO.cgo Makefile

%.o : %.cpp $(wildcard *.h) Makefile
	$(CC) $(CFLAGS) -c $(@:.o=.cpp) -o $@

CONVERT_TXT_TO_CGO=sed -e 's/"/\\"/g' -e 's/^/    << "/' -e 's/$$/" << endl/'
%.cgo: %.txt
	$(CONVERT_TXT_TO_CGO) $(@:.cgo=.txt) > $@

liblua/liblua.a: $(wildcard liblua/*.h) $(wildcard liblua/*.c)
	make -C ./liblua/ PLAT=posix liblua.a

.PHONY: clean
clean:
	-rm -f $(wildcard *.o) $(wildcard *.cgo) $(ALL_TARGETS)
	make -C ./liblua/ clean

