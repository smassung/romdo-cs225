OVERVIEW: Romdo (c) Jack Toole
  Romdo is a tool for manipulating text tables

USAGE: romdo <mainTable> <command> [parameters]

COMMANDS:
  export <saveToTable>
      Exports the values of all columns (without formulas) to <saveToTable>
        in CSV format
  list
      Display all keys in <mainTable>
  merge [table1 ... [tableN]]
      Merge the tables in [table1..tableN] into <mainTable>
  mergehigher [table1 ... [tableN]]
      Merge the tables in [table1..tableN] into <mainTable>
      If records already exist in <mainTable>, the higher of the old record
        and new record is used.
  show [key1 ... [keyN]]
      Display the record with each key in [key1..keyN] from <mainTable>
  showall <column>
      Displays all records from <column>
  sort <column>
      Displays all records from <column>, reverse-sorted
  update <key> <column> <newValue>
      Sets column <column> in record <key> to value <newValue>
  updateall <column> <newValue>
      Sets column <column> in all records to value <newValue>
  deletecolumn <column>
      Removes a column from the main table.
  histogram <column>
      Creates a histogram of the numeric values in <column> and
        displays basic statistics info
  cluster <column> <numClusters>
      Cluster the scores from column <column> into <numClusters>
        clusters.

OPTIONS:
  --help  Display this help

