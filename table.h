#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <map>
#include <set>
#include <utility>

#include "record.h"
#include "util.h"

/**
 * Represents a table keyed by it's left-most element.
 * A table consists of a set of column headers and a set of records which have
 * columns for each header.
 */
class Table
{
    public:
    // Public constants
    static const char delimiter;

    public:
    Table() { }
    explicit Table(const string & filename) { readFromFile(filename); }

    // Reading/Writing functions
    void readFromStream(istream & in);
    void readFromFile(const string & filename);
    void writeToStream(ostream & out, const string & emptyValue = Record::emptyValue,
            const char delim = Table::delimiter);
    void writeToFile(const string & filename, const string & emptyValue = Record::emptyValue,
            const char delim = Table::delimiter);

    // Data manipulation functions
    void addHeader(const string & header);
    void removeHeader(const string & header);
    void mergeRecordsFrom(const Table & otherDb, bool mergeIfHigher);
    Table evaluate() const;
    void insert(const Record & rec, bool mergeIfHigher = false);
    Record & get(const string & key) { return records[key]; }
    const Record & get(const string & key) const;
    vector<string> keys() const;
    void clear();
    bool empty() { return (key_ == ""); }

    private:
    // Typedefs for easy iterator access
    typedef set<string> HeaderSet;
    typedef map<string, Record> RecordMap;
    
    // Static constants
    static const Record emptyRecord;

    // Private helper functions
    void writeHeader(ostream & out, const char delim = Table::delimiter);
    void writeRecord(ostream & out, const Record & rec, const string & emptyValue = Record::emptyValue,
            const char delim = Table::delimiter);
    Record parseRecord(const string & line, const vector<string> headervec);
    void mergeHeaders(const HeaderSet & otherHeaders);

    // Member variables
    string key_;
    HeaderSet headers;
    RecordMap records;
};

#endif // DATABASE_H

