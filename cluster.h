/**
 * @file cluster.h
 * @author Sean Massung
 */

#ifndef _CLUSTER_H_
#define _CLUSTER_H_

#include <iostream>
#include <algorithm>
#include <string>
#include <utility>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::pair;
using std::make_pair;
using std::string;

/**
 * Represents a cluster of (student, grade) for use in a clustering algorithm.
 */
class Cluster
{
    public:

        /**
         * Creates a cluster with one element.
         */
        Cluster(const string & name, double value);

        /**
         * Creates a cluster with one element.
         */
        Cluster(pair<string, double> elem);

        /**
         * Creates a cluster based on a list of elements.
         */
        Cluster(const vector<pair<string, double>> & elems);

        /**
         * Combines this cluster with another cluster.
         * @param other The cluster to merge with
         */
        void mergeWith(const Cluster & other);

        /**
         * @return the centroid of this cluster
         */
        double average() const;

        /**
         * @return the number of elements in this cluster
         */
        size_t numElements() const;

        bool operator<(const Cluster & other) const;

        void print() const;

    private:

        void calcAvg();

        double _avg;
        vector<pair<string, double>> _elems;
};

#endif
